DemoErpExtension
=================

Requirements
------------

- Web Project for Nette 3.1 
- PHP 8.1|8.2

Installation
------------

Composer
1. add repository:


    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/ludek.balvin/demoerpextension.git"
        }
    ],

2. add extension


    composer require lb/demo-erp-extension

3. common.neon


    extensions:
	    DemoErpExtension: LB\DemoErpExtension\Config\Extension

    DemoErpExtension:
        erpEndpoint: SameModuleSamePresenterSameAction
        erpSecret: ToMuchStrongSecret
        erpVersion: 1.5.5
