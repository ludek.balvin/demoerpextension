<?php declare(strict_types = 1);

namespace LB\DemoErpExtension\Data;

/**
 * Class TestData
 *
 * @copyright 	Luděk Balvín
 * @package  	LB\DemoErpExtension\Data
 * @author		Luděk Balvín
 * @since      	26.11.2023
 */
final class TestData
{
	/**
	 * @param string $erpEndpoint
	 * @param string $erpSecret
	 * @param string $erpVersion
	 */
	public function __construct(
		public string $erpEndpoint,
		public string $erpSecret,
		public string $erpVersion
	)
	{}
}
