<?php declare(strict_types = 1);

namespace LB\DemoErpExtension\Config;

use LB\DemoErpExtension\Client;
use Nette\DI\CompilerExtension;
use Nette\Schema\Expect;
use Nette\Schema\Schema;
use stdClass;

/**
 * Extension
 * @copyright Luděk Balvín
 * @package LB\DemoErpExtension\Config
 * @author Luděk Balvín
 * @since 26.11.2023
 */
final class Extension extends CompilerExtension
{
	public const ERP_ENDPOINT = 'erpEndpoint';
	public const ERP_SECRET = 'erpSecret';
	public const ERP_VERSION = 'erpVersion';

	/** @inheritDoc */
	public function getConfigSchema(): Schema
	{
		return Expect::structure([
			self::ERP_ENDPOINT => Expect::string(),
			self::ERP_SECRET => Expect::string(),
			self::ERP_VERSION => Expect::string(),
		]);
	}

	/** @inheritDoc */
	public function loadConfiguration(): void
	{
		$builder = $this->getContainerBuilder();

		/** @var StdClass $config */
		$config = $this->getConfig();

		$builder->addDefinition($this->prefix('demoErpExtension'))
			->setFactory(Client::class,
				[
					$config->erpEndpoint,
					$config->erpSecret,
					$config->erpVersion
				]
			);
	}
}
