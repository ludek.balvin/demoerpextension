<?php declare(strict_types = 1);

namespace LB\DemoErpExtension;

use LB\DemoErpExtension\Data\TestData;

/**
 * Class by assignment
 *
 * @copyright 	Luděk Balvín
 * @package  	LB\DemoErpExtension
 * @author		Luděk Balvín
 * @since      	26.11.2023
 */
final class Client
{
	/**
	 * Constructor
	 * @param string $erpEndpoint
	 * @param string $erpSecret
	 * @param string $erpVersion
	 */
	public function __construct(
		private string $erpEndpoint,
		private string $erpSecret,
		private string $erpVersion,
	)
	{}

	/**
	 * callTest
	 * @return TestData
	 */
	public function callTest(): TestData
	{
		return new TestData(
			$this->erpEndpoint,
			$this->erpSecret,
			$this->erpVersion
		);
	}
}
