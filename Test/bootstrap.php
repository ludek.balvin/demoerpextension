<?php declare(strict_types=1);
/**
 * Bootstrap for tests
 * DemoErpExtension - boothstrap.php
 * @author Ludek Balvin info@balvin.eu
 * @package ${NAMESPACE}
 * @since 27.11.2023
 */

require __DIR__ . '/../vendor/autoload.php';

Tester\Environment::setup();

