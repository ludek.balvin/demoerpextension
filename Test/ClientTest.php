<?php declare(strict_types=1);

/**
 * DemoErpExtension - ClientTest.php
 * @author Ludek Balvin info@balvin.eu
 * @package LB\Test
 * @since 27.11.2023
 */

namespace LB\Test;

use LB\DemoErpExtension\Client;
use Tester\Assert;

require __DIR__ . '/bootstrap.php';

$client = new Client('aaa', 'bbb', 'ccc');
$testData = $client->callTest();

Assert::same('aaa', $testData->erpEndpoint);
Assert::same('bbb', $testData->erpSecret);
Assert::same('ccc', $testData->erpVersion);
